fetch('https://jsonplaceholder.typicode.com/todos')
	.then((response) => response.json())
	.then((json) => console.log(json));

// ========================================================================================

ese = [];
fetch("https://jsonplaceholder.typicode.com/todos")
// Use the 'json' method from the 'response' object to convert data retrieve into JSON format to be used in oir application
  .then((response) => 
  		response.json())
	// Print the converted JSON value from the "fetch" request
	// Using multiple "then" methods creates a "promise chain"
  .then((data) => {
    	data.map((todo) => 
    		ese.push(todo.title));
  });
console.log(ese);

// ========================================================================================

fetch('https://jsonplaceholder.typicode.com/todos/1')
	.then((response) => 
			response.json())
	.then((json) => 
			console.log(json));

const url = 'https://jsonplaceholder.typicode.com/todos/1'
async function toConsole () {
	const response = 
		await fetch(url);
	const data = 
		await response.json();
	console.log('The item "' + data.title + '" on the list has a status of ' + data.completed + '.');
}
toConsole();

// ========================================================================================

fetch('https://jsonplaceholder.typicode.com/todos', {

	// Sets the header data of the "request" object to be sent to the backend
	// Specified that the content will be in a JSON structure.
	method: 'POST',

	// Sets the header data of the "Request" object to be sent to the backend
	// Specified that the content will be in a JSON structure
	headers: {
		'Content-Type': 'application/json'
	},

	// Sets the content/body data of the "Request" object to be sent to the backend
	// JSON.stringify converts the object data into a stringified JSON
	body: JSON.stringify({
		userId: 1,
		id: 201,
		title: "I can do this",
		completed: false
	})
}).then((response) => response.json())
.then((json) => console.log(json));

// ========================================================================================

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
			dateCompleted: "Pending",
			description: "To update the my to do list with a different data structure",
	  	id: 1,
	  	status: 'Pending',
	  	title: 'JC Uploaded this',
	  	userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// ========================================================================================

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
			completed: false,
			dateCompleted: "07/09/21",
	  	id: 1,
	  	status: 'Complete',
	  	title: 'JC Updated this',
	  	userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// ========================================================================================

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
			completed: false,
			dateCompleted: "07/09/22",
	  	id: 1,
	  	status: 'Complete',
	  	title: 'JC Updated this again',
	  	userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// ========================================================================================

fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'DELETE'
});